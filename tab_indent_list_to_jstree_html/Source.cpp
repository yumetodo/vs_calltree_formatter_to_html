﻿#include <fstream>
#include <iostream>
#include <string>
#include <codecvt>
using std::size_t;
std::wstring make_indented_tag(size_t indent_num, const wchar_t* tag) {
	return std::wstring(indent_num, L'\t') + tag;
}
std::wstring insert_li_tag(std::wstring str) {
	using namespace std::string_literals;
	str.reserve(str.size() + 160);
	const size_t first_pos = str.find_first_not_of(L'\t');
	str.insert(
		first_pos, 
		LR"(<li data-jstree='{"icon":"https://lh3.googleusercontent.com/-9jGjiPN8qhg/VvURIKySvBI/AAAAAAAALuE/z5gD083HNdMZRTWXASnDRW-Smgx62cmigCCo/s800-Ic42/box.png"}'>)"
	);
	return str + L"</li>";
}
int main(int argc, char const *argv[]) {
	if (argc != 2) return -1;
	try {
		using namespace std::string_literals;
		std::wifstream in(argv[1]);
		in.imbue(std::locale(std::locale(""), new std::codecvt_utf8_utf16<wchar_t, 0x10ffff, std::consume_header>()));
		if (!in) throw std::runtime_error("err");
		std::wofstream o(argv[1] + ".html"s);
		o.imbue(std::locale(std::locale(""), new std::codecvt_utf8_utf16<wchar_t>()));
		if (!o) throw std::runtime_error("err");

		std::size_t pre_indent_num = 0, cu_indent_num = 0;
		for (std::wstring buf; std::getline(in, buf); ) {
			cu_indent_num = buf.find_first_not_of('\t');
			//if(cu_indent_num < pre_indent_num)
		}
	}
	catch (const std::exception& er) {
		std::cerr << er.what() << std::endl;
	}
}